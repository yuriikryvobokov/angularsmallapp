import { Component } from '@angular/core';
import { UsersServiceService } from './users-service.service';
import users from './_files/users.json';

interface USERS {
  _id: String;
  age: Number;
  address: String;
  company: String;
  email: String;
  name: String
}

@Component({
  selector: 'app-root',
  /*template:`<div>
  <nav>
  <a routerLink="/users">Users</a>
  </nav>
  <router-outlet></router-outlet>
  </div>`*/
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'BeKey';
 // isClicked : boolean = true;
  usersList: USERS[] = users;
  /*userDetail() {
    this.isClicked = !this.isClicked;
  }*/

  /*constructor(service: UsersServiceService) {
    service.fetchUsers("Hello world");
  }*/
  
}
