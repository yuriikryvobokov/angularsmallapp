import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h3>Not Found</h3>`
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
