import { Component, OnInit } from '@angular/core';
import users from '../_files/users.json';

interface DETAIL {
  balance: String;
  picture: String;
  age: Number;
  email: String;
  phone: String;
  address: String;
  about: String;
  tags: Object;
  registered: String; //constructor -> new Date().toTimeString()
}

@Component({
  selector: 'app-users',
  template: `<h3>Users</h3>`
})
export class UsersComponent implements OnInit {
  detailList: DETAIL[] = users;
  constructor() { }

  ngOnInit(): void {
  }

}
